#!/bin/bash
# script to extract feature with openFace for one video
# it creates a temprory directory where feature are extracted and then copy the  csv containing the feature as a .openFace_feature file
# arguments are :
# * directory where the file is
# * name of the file to process

mkdir tmp_feature_dir

echo >> Processing "'$1/$2'"



filename=$(basename -- "$2")
extension="${filename##*.}"
filename_no_ext="${filename%.*}"

right_participant_file = filename_no_ext
echo $filename_no_ext
ffmpeg -i "$1/$2" -filter:v "crop=in_w/2:in_h:in_w/2:in_h" -c:a copy "tmp_feature_dir/${filename_no_ext}_right_participant.avi"
ffmpeg -i "$1/$2" -filter:v "crop=in_w/2:in_h:0:in_h" -c:a copy "tmp_feature_dir/${filename_no_ext}_left_participant.avi"

./external_libs/openFace/OpenFace/build/bin/FeatureExtraction -f "tmp_feature_dir/${filename_no_ext}_right_participant.avi" -out_dir tmp_feature_dir
./external_libs/openFace/OpenFace/build/bin/FeatureExtraction -f "tmp_feature_dir/${filename_no_ext}_left_participant.avi" - out_dir tmp_feature_dir


mv "tmp_feature_dir/${filename_no_ext}_right_participant.csv" "$3"/"${filename_no_ext}_right_participant_openFace_feature.csv"

mv "tmp_feature_dir/${filename_no_ext}_left_participant.csv" "$3"/"${filename_no_ext}_left_participant_openFace_feature.csv"

rm -r tmp_feature_dir