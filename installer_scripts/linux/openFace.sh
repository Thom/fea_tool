#!/bin/bash
# script to install openFace

mkdir external_libs
cd external_libs
mkdir openFace
cd openFace
git clone https://github.com/TadasBaltrusaitis/OpenFace.git
cd OpenFace
# fix ubuntu version
sed -i '27s/.*/ /' install.sh
sed -i '29s/.*/if [[ `lsb_release -rs` < "18.04" ]]/' install.sh
sed -i '40s/.*/if [[ `lsb_release -rs` < "18.04" ]]; then  /' install.sh
sudo bash ./install.sh
bash download_models.sh 
cp lib/local/LandmarkDetector/model/patch_experts/*.dat build/bin/model/patch_experts/