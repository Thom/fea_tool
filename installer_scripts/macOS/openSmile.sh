#!/bin/bash
# script to install openSmile
mkdir external_libs
cd external_libs
mkdir openSmile
cd openSmile
git clone https://github.com/audeering/opensmile.git

cd opensmile/
bash build.sh 
export PATH=$PWD/build/progsrc/smilextract:$PATH
echo $PATH
echo "openSmile installed"