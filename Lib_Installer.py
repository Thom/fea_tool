import sys
from PyQt5.QtCore    import *
from PyQt5.QtWidgets import *
import pandas as pd
from PyQt5.QtCore import QDir, QProcess, QSize, Qt, QUrl
from PyQt5.QtGui import  QTextCursor
import getpass
import re
import pexpect 
import sys, os, platform

# import SequentialManager as sm
class Libs_Installer(QWidget):
    def __init__(self, parent=None):
        super(Libs_Installer, self).__init__(parent)
        
        self.grid = self.update_ui()
        self.button = QPushButton("Install selected libraries")
        self.button.clicked.connect(self.installLib)
        pw_label = QLabel("Password : (privileges needed to install some libraries) ")
        self.pw = QLineEdit()
        self.pw.setEchoMode(QLineEdit.Password)
    
        
        self.progressBar = QProgressBar(self)
        self.progressBar.setTextVisible(False)

        
        self.pswLayout = QHBoxLayout()
        self.pswLayout.addWidget(pw_label)
        self.pswLayout.addWidget(self.pw)


        self.mainLayout = QVBoxLayout(self)
        self.mainLayout.addLayout(self.grid)
        self.mainLayout.addStretch()
        self.mainLayout.addLayout(self.pswLayout)
        self.mainLayout.addWidget(self.button)
        self.mainLayout.addWidget(self.progressBar)
        
        self.setLayout(self.mainLayout)

        
    def update_ui(self):
        self.installers_data_json = pd.read_json('installers_data.json')
        self.installers_df = self.installers_data_json.transpose()
        
        self.listInstallers = self.installers_df.index.values
        self.listInstalled    = self.installers_df.is_installed.values
        self.listCheckBox = ['']*len(self.listInstallers)
        grid = QGridLayout()
        self.library_name = QLabel("Library")
        self.is_installed_name = QLabel("is installed")
        self.to_install_name = QLabel("install the library")
        grid.addWidget(self.library_name, 0, 0)
        grid.addWidget(self.is_installed_name, 0, 1)
        grid.addWidget(self.to_install_name, 0, 2)
        for i, v in enumerate(self.listInstallers):
            self.listInstallers[i] = QLabel(v)
            self.listInstalled[i] = QLabel("True" if self.listInstalled[i] else "False")
            self.listCheckBox[i] = QCheckBox(v)
            grid.addWidget(self.listInstallers[i], i+1, 0)
            grid.addWidget(self.listInstalled[i],    i+1, 1)
            grid.addWidget(self.listCheckBox[i],    i+1, 2)

        return grid


    def installLib(self):
        # cmds = []
        password = self.pw.text()
        for i, v in enumerate(self.listInstallers):
            if self.listCheckBox[i].isChecked():
                library = v.text()
                print("install " + str(library))

                os_installation_script = {'Linux' : 'linux_installation_script', 'Darwin' : 'macOS_installation_script', 'Windows' : 'windows_installation_script'}
                
                os = str(platform.system())
                print("os " + os)
                cmd = str("sudo bash " + str(self.installers_data_json[library][os_installation_script.get(os)]))

                print('cmd : '+ str(cmd))
                exit_code = self.run_script(cmd, password)
                print('exit_code : '+ str(exit_code))
                if exit_code == 0:
                    self.listInstalled[i] = QLabel("True")
                    self.listCheckBox[i].setChecked(False)
                    self.installers_data_json = pd.read_json('installers_data.json')
                    self.installers_data_json[library].is_installed = True
                    self.installers_data_json.to_json('installers_data.json')
                    self.update_ui()

                
    def run_script(self, cmd, password):
        child = pexpect.spawn(cmd, timeout=None, encoding='utf-8')
        child.logfile = sys.stdout
        progressBarCompleted = 0
        while True:
            try:
                index = child.expect(['Password:.*', '\n', '.*:  \d*% .*'])

                if index == 0:
                    # # if it's found, send the password
                    child.sendline(password)
                else:
                    progressBarCompleted = progressBarCompleted+1 if progressBarCompleted<99 else 0
                    self.progressBar.setValue(progressBarCompleted)
                
            except pexpect.EOF:
                child.close()
                print(child.exitstatus, child.signalstatus)
                if os.WIFEXITED(child.exitstatus) :
                    code = os.WEXITSTATUS(child.exitstatus)
                    return code
                else :
                    return -1
                break 
            
        

    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = Libs_Installer()
    clock.show()
    sys.exit(app.exec_())
